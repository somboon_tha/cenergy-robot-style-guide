*** Setting ***
Resource    ${CURDIR}/../../../resources/imports.robot
Resource    ${CURDIR}/../page/login_page.robot
Resource    ${CURDIR}/../page/welcome_page.robot

*** Keywords ***
Login web
    [Documentation]    Login with username and password success
    [Arguments]    ${user}   ${pass}
    Input username    ${user}
    Input password    ${pass}
    Click login button
    Check go to welcome page

Login web unsuccess
    [Documentation]    Login with username and password success
    [Arguments]    ${user}   ${pass}
    Input username    ${user}
    Input password    ${pass}
    Click login button
    Check error label appear