*** Setting ***
Library      RequestsLibrary
Library      Collections
Library      DateTime
Library      Selenium2Library
# Resource    keywords/common_keywords.robot
Variables    configs/${ENV}/env_config.yaml
Variables    testdata/common/test_data.yaml
Variables    common_configs.yaml